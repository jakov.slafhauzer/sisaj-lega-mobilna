package com.example.hair.ui.theme

import androidx.compose.ui.graphics.Color


val Black = Color(0xFF000000)
val Grey = Color(0xFF808080)
val Brown = Color(0xFFAF5D05)
val Orange = Color(0xFFFF9900)