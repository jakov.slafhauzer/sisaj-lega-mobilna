package com.example.hair.data

data class Hairstyle(
    val id: String = "",
    val name: String = "",
    val description: String = "",
    val images: List<String> = emptyList(), // List of image URLs
    val type: String = ""
)
