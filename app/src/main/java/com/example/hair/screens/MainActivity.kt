package com.example.hair.screens

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.hair.data.Hairstyle
import com.example.hair.ui.theme.HairTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialize Firebase
        FirebaseApp.initializeApp(this)

        setContent {
            HairTheme {
                MyApp()
            }
        }
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter", "UnusedMaterial3ScaffoldPaddingParameter")
    @Composable
    fun MyApp() {
        val navController = rememberNavController()
        Scaffold(
            bottomBar = {
                BottomNavigationBar(navController)
            }
        ) {
            NavHost(navController = navController, startDestination = "home") {
                composable("home") { HomeScreen(navController) }
                composable("Kratke") { HairstyleListScreen("Kratke", navController) }
                composable("Duge") { HairstyleListScreen("Duge", navController) }
                composable("Prijelazi") { HairstyleListScreen("Prijelazi", navController) }
                composable("details/{id}") { backStackEntry ->
                    val id = backStackEntry.arguments?.getString("id")
                    DetailsScreen(id ?: "", navController)
                }
            }
        }
    }

    @Composable
    fun BottomNavigationBar(navController: NavController) {
        val currentDestination = navController.currentBackStackEntryAsState().value?.destination?.route
        val colorScheme = MaterialTheme.colorScheme // Get the current color scheme from your theme

        BottomNavigation(
            backgroundColor = colorScheme.primary, // Set the background color
            contentColor = colorScheme.onPrimary    // Set the content (icon/text) color
        ) {
            BottomNavigationItem(
                icon = { Text("Početna") },
                selected = currentDestination == "home",
                onClick = { navController.navigate("home") },
                selectedContentColor = colorScheme.secondary, // Color for the selected item
                unselectedContentColor = colorScheme.onPrimary.copy(alpha = 0.6f) // Color for unselected items
            )
            BottomNavigationItem(
                icon = { Text("Kratke") },
                selected = currentDestination == "Kratke",
                onClick = { navController.navigate("Kratke") },
                selectedContentColor = colorScheme.secondary,
                unselectedContentColor = colorScheme.onPrimary.copy(alpha = 0.6f)
            )
            BottomNavigationItem(
                icon = { Text("Duge") },
                selected = currentDestination == "Duge",
                onClick = { navController.navigate("Duge") },
                selectedContentColor = colorScheme.secondary,
                unselectedContentColor = colorScheme.onPrimary.copy(alpha = 0.6f)
            )
            BottomNavigationItem(
                icon = { Text("Prijelazi") },
                selected = currentDestination == "Prijelazi",
                onClick = { navController.navigate("Prijelazi") },
                selectedContentColor = colorScheme.secondary,
                unselectedContentColor = colorScheme.onPrimary.copy(alpha = 0.6f)
            )
        }
    }

    @Composable
    fun HomeScreen(navController: NavController) {
        var searchQuery by remember { mutableStateOf("") }
        var searchResults by remember { mutableStateOf(listOf<Hairstyle>()) }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            Text(text = "ŠIŠAJ LEGA!", style = MaterialTheme.typography.headlineMedium)

            // Search Bar
            OutlinedTextField(
                value = searchQuery,
                onValueChange = { query ->
                    searchQuery = query
                    searchHairstyles(query) { results ->
                        searchResults = results
                    }
                },
                label = { Text("Pronađi frizuru...") },
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(8.dp))

            // Display search results or main categories if search is empty
            if (searchQuery.isEmpty()) {
                val categories = listOf("Kratke", "Duge", "Prijelazi")
                LazyColumn {
                    items(categories) { category ->
                        CategoryItem(category, navController)
                    }
                }
            } else {
                LazyColumn {
                    items(searchResults) { hairstyle ->
                        HairstyleItem(hairstyle, navController)
                    }
                }
            }
        }
    }

    fun searchHairstyles(query: String, onResult: (List<Hairstyle>) -> Unit) {
        val firestore = FirebaseFirestore.getInstance()
        firestore.collection("hairstyles")
            .get()
            .addOnSuccessListener { documents ->
                val queryLowercase = query.lowercase()
                val hairstyles = documents.map { it.toObject(Hairstyle::class.java) }
                    .filter { it.name.lowercase().contains(queryLowercase) }
                onResult(hairstyles)
            }
            .addOnFailureListener { exception ->
                // Handle the error, e.g., log the exception
                onResult(emptyList())
            }
    }

    @Composable
    fun CategoryItem(category: String, navController: NavController) {
        val coroutineScope = rememberCoroutineScope()
        var hairstyles by remember { mutableStateOf(listOf<Hairstyle>()) }

        LaunchedEffect(category) {
            coroutineScope.launch {
                hairstyles = getHairstyles(category)
            }
        }

        Column(
            modifier = Modifier
                .padding(8.dp)
                .clickable {
                    val route = when (category) {
                        "Kratke" -> "Kratke"
                        "Duge" -> "Duge"
                        "Frizure" -> "Prijelazi"
                        else -> "home" // fallback
                    }
                    navController.navigate(route)
                }
        ) {
            Text(text = category, style = MaterialTheme.typography.titleMedium)

            LazyRow {
                items(hairstyles.shuffled().take(5)) { hairstyle ->
                    Image(
                        painter = rememberImagePainter(hairstyle.images.firstOrNull()),
                        contentDescription = null,
                        modifier = Modifier
                            .size(100.dp)
                            .padding(4.dp),
                        contentScale = ContentScale.Crop
                    )
                }
            }
        }
    }

    @Composable
    fun HairstyleListScreen(type: String, navController: NavController) {
        val coroutineScope = rememberCoroutineScope()
        var hairstyles by remember { mutableStateOf(listOf<Hairstyle>()) }

        LaunchedEffect(type) {
            coroutineScope.launch {
                hairstyles = getHairstyles(type).distinctBy { it.name }
            }
        }

        LazyColumn {
            items(hairstyles) { hairstyle ->
                HairstyleItem(hairstyle, navController)
            }
        }
    }

    @Composable
    fun HairstyleItem(hairstyle: Hairstyle, navController: NavController) {
        Column(
            modifier = Modifier
                .padding(8.dp)
                .clickable {
                    navController.navigate("details/${hairstyle.id}")
                }
        ) {
            Text(text = hairstyle.name, style = MaterialTheme.typography.titleMedium)
            Row {
                hairstyle.images.take(3).forEach { imageUrl ->
                    Image(
                        painter = rememberImagePainter(imageUrl),
                        contentDescription = null,
                        modifier = Modifier
                            .size(100.dp)
                            .padding(4.dp),
                        contentScale = ContentScale.Crop
                    )
                }
            }
        }
    }

    @Composable
    fun DetailsScreen(id: String, navController: NavController) {
        val coroutineScope = rememberCoroutineScope()
        var hairstyle by remember { mutableStateOf<Hairstyle?>(null) }

        LaunchedEffect(id) {
            coroutineScope.launch {
                hairstyle = getHairstyleById(id)
            }
        }

        hairstyle?.let {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                Text(text = it.name, style = MaterialTheme.typography.headlineMedium)
                Spacer(modifier = Modifier.height(8.dp))
                Text(text = it.description, style = MaterialTheme.typography.bodyMedium)

                Spacer(modifier = Modifier.height(16.dp))

                // LazyRow for horizontal scrolling of images
                LazyRow(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp), // Adjust height as needed
                    contentPadding = PaddingValues(horizontal = 8.dp),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    items(it.images) { imageUrl ->
                        Image(
                            painter = rememberImagePainter(imageUrl),
                            contentDescription = null,
                            modifier = Modifier
                                .size(150.dp) // Adjust size as needed
                                .clip(MaterialTheme.shapes.medium),
                            contentScale = ContentScale.Crop
                        )
                    }
                }

                Spacer(modifier = Modifier.height(16.dp))

                Button(
                    onClick = { navController.popBackStack() },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = Color.White
                    ),
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                ) {
                    Text(text = "Povratak")
                }
            }
        }
    }

    suspend fun getHairstyles(type: String): List<Hairstyle> {
        return try {
            FirebaseFirestore.getInstance().collection("hairstyles")
                .whereEqualTo("type", type)
                .get()
                .await()
                .documents
                .map { document ->
                    document.toObject(Hairstyle::class.java)!!
                }
        } catch (e: Exception) {
            emptyList()
        }
    }

    suspend fun getHairstyleById(id: String): Hairstyle? {
        return try {
            FirebaseFirestore.getInstance().collection("hairstyles")
                .document(id)
                .get()
                .await()
                .toObject(Hairstyle::class.java)
        } catch (e: Exception) {
            null
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MainActivity().MyApp()
}
